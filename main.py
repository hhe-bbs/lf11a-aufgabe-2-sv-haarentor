from classes.sportverein import Sportverein
from classes.gruppe import Gruppe
from classes.mitglied import Mitglied

mitglieder = []
mitglieder.append(Mitglied(vorname="Fritz", nachname="Nee", id=76136))
mitglieder.append(Mitglied(vorname="Sussy", nachname="Baka", id=81642))
mitglieder.append(Mitglied(vorname="Nee", nachname="Eyy", id=98120))
mitglieder.append(Mitglied(vorname="Bablo", nachname="Nasenbär", id=53841))
mitglieder.append(Mitglied(vorname="Aha", nachname="Dings", id=81730))
mitglieder.append(Mitglied(vorname="Karl", nachname="Mak", id=49182))
mitglieder.append(Mitglied(vorname="Pinguin", nachname="Tux", id=42420))
mitglieder.append(Mitglied(vorname="Ach", nachname="Duuu", id=91834))

verein = Sportverein(mitglieder=mitglieder, name="Sportlich saufen SV", eMail="mal-anderer@sport.de", IBAN="DE8030812648102748127400")

