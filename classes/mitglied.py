class Mitglied:
    def __init__(self, vorname:str, nachname:str, id:int) -> None:
        self.id = id
        self.vorname = vorname
        self.nachname = nachname

    def __CheckValidID(self):
        if len(self.id) != 5:
            return 1